#include <stdio.h>

#define bitnum(t, size)	      \
  { \
  t n; \
  int i; \
  n = 0; \
  n = ~n; \
  i = 0; \
  while (n) { \
  ++i; \
  n <<= 1; \
  } \
  size = i;		\
  }

int chsign()
{
  return ~(char)0;
}


int main ()
{
  int isize;
  int bsize;
  char* sign;
  char* is;
  
  sign = chsign() < 0 ? "signed" : "unsigned";
  bitnum(char, bsize);
  bitnum(int, isize);

#ifndef CC
  #define CC "unknown"
#endif
#ifndef SYSTEM
  #define SYSTEM "unknown"
#endif

  puts("Information about implementation of C language on current system/architecture environment.\n");
  printf("System   : %s\n"
	 "Compiler : %s\n\n",
	 SYSTEM, CC);

  printf("Byte has %d bits\n"
	 "Word has %d bits\n",
	 bsize, isize);
  is = sizeof(char*) == sizeof(int*)
    ? "is" : "is not";
  printf("Address has %d bits\n"
	 "Address of a byte has %d bits\n"
	 "So byte %s directly addressable.\n\n",
	 sizeof(int*) * bsize,
	 sizeof(char*) * bsize,
	 is);

  puts("In C language char type size is the same as byte of the machine. It is the smallest addressable memory which is at least 8-bit long. It can also be longer.\n\nFor example on PDP-10 word size is 36 bit. Also the word is the smallest addresable memory. That computer has no notion of a byte. To implement char type the simplest way is to have char type being 36-bit. The same as int as int coresponds to word size. However this would be inneficient. Other solution is to have char pointer being longer than pointer to int. Then char* would keep address of a word plus offset from beginning of the word. This would allow to have char of a size of 9-bits. But it is also posiblle to have 12 or 18 bit char on this machine. Word must be multiple of char size.\n");
  puts("Pre ANSI C types (aka K&R C, years 1973 - 1988)");
  printf("char  is %d-bit long, %s\n",
	 bsize, sign);
  printf("short is %d-bit\n"
	 "int   is %d-bit\n"
	 "long  is %d-bit\n",
	 sizeof(short) * bsize,
	 sizeof(int) * bsize,
	 sizeof(long) * bsize);
  printf("char* is %d-bit\n"
	 "int*  is %d-bit\n",
	 sizeof(char*) * bsize,
	 sizeof(int*) * bsize);

#ifdef __STDC__
  puts("Compiler is ANSI C compilant");
#endif

  return 0;
}
